G-FranC is available for researchers and data scientists under the Creative Commons BY license.
In case of publication and/or public use of G-FranC, as well as any dataset derived from it, one should acknowledge its creators by citing the following paper.

@inproceedings{G-FranC,
    title    = {G-FranC: a dataset of criminal activities mapped as a complex network in a relational DBMS},
    author   = {Scabora, L. C. and
                Spadon, G. and
                Rodrigues, L. S. and
                Cazzolato, M. T. and
                Ara{\'{u}}jo, M. V. S. and
                Sousa, E. P. M. and
                Traina, A. J. M. and
                Rodrigues-Jr., J. F. and
                Traina-Jr., C.},
    booktitle = {Dataset Show Case of the 34th Brazilian Symposium on Databases},
    publisher = {SBC},
    pages     = {366--376},
    year      = {2019}
}
